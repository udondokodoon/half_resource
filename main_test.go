package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestMain(t *testing.T) {
	os.RemoveAll("./newRes")
	err := halfResources("./res", "./newRes")
	if err != nil {
		t.Fatalf("Fatal: %s", err)
	}

	filepath.Walk("./res", func(path string, f os.FileInfo, err error) error {
		rel, err := filepath.Rel("./res", path)
		if err != nil {
			return err
		}
		if rel == "." || rel == ".." {
			return nil
		}
		newPath := "./newRes/" + rel
		t.Logf("NewPath: %s", newPath)
		if _, err := os.Stat(newPath); os.IsNotExist(err) {
			//abs, _ := filepath.Abs(newPath)
			t.Errorf("not exists: " + newPath)
			return nil
		}
		return nil
	})
	if err != nil {
		t.Fatalf("Fatal: %s", err)
	}
}