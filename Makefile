all: build

build:
	GOOS=linux GOARCH=amd64 go build -o half_resource ./main.go
	GOOS=darwin GOARCH=amd64 go build -o half_resource ./main.go

clean:
	rm half_resource
	rm half_resource_osx