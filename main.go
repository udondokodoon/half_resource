package main

import (
	"bufio"
	"fmt"
	"github.com/nfnt/resize"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
)

var (
	regs = map[string]*regexp.Regexp{
		"pngOrJpg":           regexp.MustCompile("(?:png|jpg)$"),
		"plist":              regexp.MustCompile("plist$"),
		"armature":           regexp.MustCompile("ExportJson$"),
		"armatureInnerScale": regexp.MustCompile("\"content_scale\": ([0-9]+(?:\\.[0-9]+))"),
		"twoDim":             regexp.MustCompile("{([0-9]+?),([0-9]+?)}"),
		"integerValue":       regexp.MustCompile("<integer>([0-9]+)</integer>"),
		"halfValues":         regexp.MustCompile("<key>(?:width|height|originalWidth|originalHeight|x|y)</key>"),
	}
)

func createVisitor(baseDir string, lightPath string) func(string, os.FileInfo, error) error {

	return func(path string, f os.FileInfo, err error) error {
		relPath, err := filepath.Rel(baseDir, path)
		if err != nil {
			log.Fatal(err)
		}
		newFilePath := fmt.Sprintf("%s/%s", lightPath, relPath)
		os.MkdirAll(filepath.Dir(newFilePath), 0755)

		// PNG or JPEG
		if matched := regs["pngOrJpg"].MatchString(path); matched {
			halfImg(path, newFilePath)
			return nil
		}

		// Plist
		if matched := regs["plist"].MatchString(path); matched {
			halfPlist(path, newFilePath)
			return nil
		}

		// Armature
		if matched := regs["armature"].MatchString(path); matched {
			halfArmature(path, newFilePath)
			return nil
		}

		os.Link(path, newFilePath)
		return nil
	}
}

func halfImg(path string, lightResourcePath string) {
	log.Printf("path: %s => %s\n", path, lightResourcePath)
	fp, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	config, format, err := image.DecodeConfig(fp)
	if err != nil {
		log.Fatal(err)
	}
	fp.Seek(0, 0)

	var img image.Image

	if format == "png" {
		img, err = png.Decode(fp)
		if err != nil {
			log.Fatal(err, ":", format, ":", path)
		}
	} else {
		img, err = jpeg.Decode(fp)
		if err != nil {
			log.Fatal(err, ":", format, ":", path)
		}
	}

	resized := resize.Resize(uint(config.Width/2), uint(config.Height/2), img, resize.Bilinear)

	// 暫定
	fo, err := os.Create(fmt.Sprintf(lightResourcePath))
	if err != nil {
		log.Fatal(err)
	}
	defer fo.Close()
	if format == "png" {
		err = png.Encode(fo, resized)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		err = jpeg.Encode(fo, resized, nil)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func halfPlist(path string, lightResourcePath string) {
	log.Printf("path: %s => %s\n", path, lightResourcePath)

	fp, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	// 座標やframeサイズ等
	repl1 := func(matched string) string {
		m_ := regs["twoDim"].FindStringSubmatch(matched)
		k, _ := strconv.Atoi(m_[1])
		l, _ := strconv.Atoi(m_[2])
		return fmt.Sprintf("{%d,%d}", k/2, l/2)
	}

	repl2 := func(matched string) string {
		m_ := regs["integerValue"].FindStringSubmatch(matched)
		k, _ := strconv.Atoi(m_[1])
		return fmt.Sprintf("<integer>%d</integer>", k/2)
	}

	// 暫定
	fo, err := os.Create(fmt.Sprintf(lightResourcePath))
	if err != nil {
		log.Fatal("failed to open putput file")
	}
	defer fo.Close()
	w := bufio.NewWriter(fo)

	for scanner.Scan() {
		txt := scanner.Text()
		txt = regs["twoDim"].ReplaceAllStringFunc(txt, repl1)
		w.WriteString(txt + "\n")
		if matched := regs["halfValues"].MatchString(txt); matched {
			scanner.Scan()
			txt := scanner.Text()
			txt = regs["integerValue"].ReplaceAllStringFunc(txt, repl2)
			w.WriteString(txt + "\n")
		}
		w.Flush()
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func halfArmature(path string, lightResourcePath string) {
	log.Printf("path: %s => %s\n", path, lightResourcePath)

	fp, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)

	// 暫定
	fo, err := os.Create(fmt.Sprintf(lightResourcePath))
	if err != nil {
		log.Fatal("failed to open putput file")
	}
	defer fo.Close()
	w := bufio.NewWriter(fo)

	flag := false
	for scanner.Scan() {
		txt := scanner.Text()
		if flag {
			w.WriteString(txt + "\n")
			w.Flush()
			continue
		}

		if flag := regs["armatureInnerScale"].MatchString(txt); flag {
			txt = regs["armatureInnerScale"].ReplaceAllStringFunc(txt, func(matched string) string {
				m_ := regs["armatureInnerScale"].FindStringSubmatch(matched)
				k, _ := strconv.ParseFloat(m_[1], 32)
				return fmt.Sprintf("\"content_scale\": %f", k/2.0)
			})
		}
		w.WriteString(txt + "\n")
		w.Flush()
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

}

func halfResources(path string, lightPath string) error {
	err := filepath.Walk(path, createVisitor(path, lightPath))
	return err
}

func main() {

	/*
		cpus := runtime.numcpu()
		runtime.gomaxprocs(cpus)
	*/

	if len(os.Args) < 3 {
		log.Fatal("invalid arguments")
	}
	originalPath := os.Args[1]
	lightPath := os.Args[2]

	err := halfResources(originalPath, lightPath)
	if err != nil {
		fmt.Println("%v", err)
	}
}